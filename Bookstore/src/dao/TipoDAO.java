package dao;

import javax.ejb.Stateless;
import entidade.Tipo;

@Stateless
public class TipoDAO extends GenericDAO<Tipo> {

	public TipoDAO() {
		super(Tipo.class);
	}

}
