package dao;

import javax.ejb.Stateless;
import entidade.Genero;

@Stateless
public class GeneroDAO extends GenericDAO<Genero> {

	public GeneroDAO() {
		super(Genero.class);
	}

}