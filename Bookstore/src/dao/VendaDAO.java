package dao;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import util.PdfPageHelper;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import entidade.Livro;
import util.GraficoLinha;
import entidade.Cliente;
import entidade.Venda;
import entidade.VendaLivro;


@Stateless
public class VendaDAO extends GenericDAO<Venda> {

	public VendaDAO() {
		super(Venda.class);
	}
	@Override
	public String salvar(Venda venda) {
		try {
			
			Double total = 0.0;
			System.out.println(venda.getVendaLivros().size());
			for(VendaLivro lv : venda.getVendaLivros()){
				
				total+=lv.getValor() * lv.getQuantidade();
				lv.setVenda(venda);
				
				em.persist(lv);

			}
			
			venda.setValorTotal(total);
			
			em.persist(venda);
			em.merge(venda);
			return null;
		} catch (Exception ex) {
			ex.printStackTrace();
			return "Erro: " + ex.getMessage();
		}
	}

	public List<Venda> listar(Cliente cliente, Date data){
		System.out.println(cliente);
		TypedQuery<Venda> query = super.em.createQuery("select a from Venda a where "
				+ "a.cliente = :cliente or a.dataVenda = :data",Venda.class);
		
		query.setParameter("cliente", cliente);
		query.setParameter("data", data,TemporalType.DATE);
		
		List<Venda> vendas= query.getResultList();

		return vendas;
		
		
	}

	public List<GraficoLinha> listar(int ano,Livro livro){
		
		Query query = super.em.createQuery("select EXTRACT(MONTH from a.venda.dataVenda) as y,SUM(a.valor * a.quantidadeDia) from VendaLivro a "
				+ "where EXTRACT(YEAR from a.venda.dataVenda) =:ano and a.livro = :livro group by y");
		
		query.setParameter("ano", ano); 
		query.setParameter("livro", livro);
		
		List<GraficoLinha> vendas= new ArrayList<GraficoLinha>();
				
		@SuppressWarnings("unchecked")
		List<Object[]> list=query.getResultList();
		
		for (Object[] objects : list) {
			vendas.add(new GraficoLinha((Integer)objects[0], (Double)objects[1]));
		}

		System.out.println(vendas.size());
		return vendas;
		
		
	}

	public ByteArrayOutputStream relatorio(Cliente cliente, Date data) throws Exception{
		
		List<Venda> vendas = listar(cliente, data);
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		Document document = new Document(PageSize.A4.rotate());
		
		PdfWriter pdfWriter =PdfWriter.getInstance(document, output);
		PdfPageHelper helper = new PdfPageHelper();
		
		helper.setTitle("Relat�rio de loca��es");
		pdfWriter.setPageEvent(helper);
		
		
		document.open();
		
		Font fontCabecalho = new Font(FontFamily.TIMES_ROMAN,14,Font.BOLD);
		Font fontTexto = new Font(FontFamily.TIMES_ROMAN,12);
		
		PdfPTable table = new PdfPTable(4);
		table.setHeaderRows(1);
		table.setWidthPercentage(100);
		table.setWidths(new int[]{20,40,20,20});
		String[] colunas = {"N�mero","Cliente","Valor","Data"};
		
		for (String coluna : colunas) {
			PdfPCell cell = new PdfPCell(new Phrase(coluna,fontCabecalho));
			cell.setBackgroundColor(new BaseColor(204, 204, 204));
	        cell.setHorizontalAlignment(Element.ALIGN_CENTER) ;
			table.addCell(cell);
		}
		
		
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
		DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);
		
		double total = 0;
		for (Venda venda : vendas) {
			PdfPCell cellNumero = new PdfPCell(new Phrase(venda.getIdVenda().toString(),fontTexto));
			
			PdfPCell cellCliente= new PdfPCell(new Phrase(venda.getCliente().getNome(),fontTexto));
			
			
			PdfPCell cellValor = new PdfPCell(new Phrase(numberFormat.format(venda.getValorTotal()),fontTexto));
			
			PdfPCell cellData = new PdfPCell(new Phrase(dateFormat.format(venda.getDataVenda()),fontTexto));
		
			table.addCell(cellNumero);
			table.addCell(cellCliente);
			table.addCell(cellValor);
			table.addCell(cellData);
			
			total += total + venda.getValorTotal();
		}
		
		Paragraph paragraphTotal = new Paragraph("Valor Total = "+numberFormat.format(total),fontCabecalho);
		paragraphTotal.setAlignment(Element.ALIGN_RIGHT);
		
		
		
		document.add(table);
		document.add(paragraphTotal);
		document.close();
		
		return output;
	}

	public ByteArrayOutputStream ficha(int idVenda) throws Exception{
		
		Venda venda= super.em.find(Venda.class, idVenda);
		
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		Document document = new Document(PageSize.A4.rotate());
		
		PdfWriter pdfWriter =PdfWriter.getInstance(document, output);
		PdfPageHelper helper = new PdfPageHelper();
		
		helper.setTitle("Venda de "+venda.getCliente().getNome());
		pdfWriter.setPageEvent(helper);
		
		
		document.open();
		
		Font fontCabecalho = new Font(FontFamily.TIMES_ROMAN,14,Font.BOLD);
		Font fontTexto = new Font(FontFamily.TIMES_ROMAN,12);
		
		PdfPTable table = new PdfPTable(5);
		table.setHeaderRows(1);
		table.setWidthPercentage(100);
		table.setWidths(new int[]{30,30,15,10,15});
		String[] colunas = {"Autor","Genero","Valor","Qtde","Total"};
		
		for (String coluna : colunas) {
			PdfPCell cell = new PdfPCell(new Phrase(coluna,fontCabecalho));
			cell.setBackgroundColor(new BaseColor(204, 204, 204));
	        cell.setHorizontalAlignment(Element.ALIGN_CENTER) ;
			table.addCell(cell);
		}
		
		
		NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
	
		for (VendaLivro vendaLivro : venda.getVendaLivros()) {
			PdfPCell cellAutor = new PdfPCell(new Phrase(vendaLivro.getLivro().getAutor(),fontTexto));
			
			PdfPCell cellGenero= new PdfPCell(new Phrase(vendaLivro.getLivro().getGenero().getDescricao(),fontTexto));
			
			
			PdfPCell cellValor = new PdfPCell(new Phrase(numberFormat.format(vendaLivro.getValor()),fontTexto));
			
			PdfPCell cellQtde = new PdfPCell(new Phrase(vendaLivro.getQuantidade().toString(),fontTexto));
			
			PdfPCell cellTotal= new PdfPCell(new Phrase(numberFormat.format(vendaLivro.getValor() * vendaLivro.getQuantidade()),fontTexto));
		
			table.addCell(cellAutor);
			table.addCell(cellGenero);
			table.addCell(cellValor);
			table.addCell(cellQtde);
			table.addCell(cellTotal);
			
		}
		
		Paragraph paragraphTotal = new Paragraph("Valor Total = "+numberFormat.format(venda.getValorTotal()),fontCabecalho);
		paragraphTotal.setAlignment(Element.ALIGN_RIGHT);
		
		
		
		document.add(table);
		document.add(paragraphTotal);
		document.close();
		
		return output;
	}
	
}
