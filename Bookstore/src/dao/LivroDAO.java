package dao;

import javax.ejb.Stateless;
import entidade.Livro;

@Stateless
public class LivroDAO extends GenericDAO<Livro> {

	public LivroDAO() {
		super(Livro.class);
	}

}
