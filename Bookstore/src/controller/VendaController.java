package controller;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.CategoryAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.PieChartModel;

import dao.VendaDAO;
import dao.LivroDAO;
import entidade.Venda;
import entidade.VendaLivro;
import entidade.Livro;
import util.GraficoLinha;
import util.MensagemUtil;

@ManagedBean
@SessionScoped
public class VendaController implements Serializable{


	private static final long serialVersionUID = 1L;

	@EJB
    private VendaDAO vendaDAO;
	@EJB
    private LivroDAO livroDAO;
	
    private Venda venda;
    private VendaLivro vendaLivro;
    private Integer ano;
    
    private LineChartModel graficoLinha;
    
    private List<Venda> vendas;
    
    public VendaController(){
        venda = new Venda();
        vendaLivro = new VendaLivro();
        graficoLinha = new LineChartModel();
    }
    
    public void salvar(){
        
        String erro = vendaDAO.salvar(venda);
        
        if(erro == null){ 
            venda = new Venda();
            vendaLivro = new VendaLivro();
            MensagemUtil.addMensagemInfo("Salvo com sucesso!");
        }else{
            MensagemUtil.addMensagemInfo("Erro ao salvar: " + erro);
        }
    }
   
    public void listar(){
        vendas =  vendaDAO.listar(venda.getCliente(), venda.getDataVenda());
    }
    
    
    public void relatorio() throws Exception{
        ByteArrayOutputStream output = vendaDAO.relatorio(venda.getCliente(), venda.getDataVenda());
        
        
        HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();  
        res.setContentType("application/pdf");  

        res.setHeader("Content-disposition", "inline;filename=arquivo.pdf");     
        res.getOutputStream().write(output.toByteArray());  
      //  res.getCharacterEncoding();  
        FacesContext.getCurrentInstance().responseComplete();
    }
    
    public void ficha(int idVenda) throws Exception{
        ByteArrayOutputStream output = vendaDAO.ficha(idVenda);
        
        
        HttpServletResponse res = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();  
        res.setContentType("application/pdf");  

        res.setHeader("Content-disposition", "inline;filename=arquivo.pdf");   
        res.getOutputStream().write(output.toByteArray());  
       // res.getCharacterEncoding();  
        FacesContext.getCurrentInstance().responseComplete();
    }

    
    public void adicionar(){
    	
    	if(venda.getVendaLivros() == null){
    		venda.setVendaLivros(new ArrayList<VendaLivro>());
    	}
    	
    	boolean achou = false;
    	for (VendaLivro l : venda.getVendaLivros()) {
			
    		if(l.getLivro().getIdLivro() == vendaLivro.getLivro().getIdLivro()){
    			Short dias =  (short) (l.getQuantidade() + vendaLivro.getQuantidade());
    			l.setQuantidade(dias);
    			achou= true;
    			break;
    		}
    		
		}
    	
    	if(achou == false){
    		venda.getVendaLivros().add(vendaLivro);
    	}
    	
    	vendaLivro = new VendaLivro();
    }
    
    public PieChartModel graficoPorLivro() {
    	PieChartModel model = new PieChartModel();
    	
    	
    	List<Livro> livros = livroDAO.todos();
    	
    	for (Livro livro : livros) {
    		model.set(livro.getAutor() + " - "+livro.getGenero().getDescricao(),
    				livro.getVendaLivros().size());
		}
         
    	
         
    	model.setTitle("Quantidade de loca��es por ve�culo");
    	model.setLegendPosition("e");
    	model.setShowDataLabels(true);
   
    	return model;
    }
    
    
    public void graficoLinha() {
    	
    	graficoLinha = new LineChartModel();
    	List<Livro> livros = livroDAO.todos();
    	graficoLinha.getAxes().put(AxisType.X, new CategoryAxis("Meses"));
    	for (Livro livro : livros) {
    		 LineChartSeries series = new LineChartSeries();
    		 series.setLabel(livro.getAutor()+ " - "+livro.getGenero().getDescricao());
    		 
    		 List<GraficoLinha> linhas = vendaDAO.listar(ano, livro);
    		 
    		 for (GraficoLinha graficoLinha : linhas) {
    			 series.set(graficoLinha.getMes().toString(), graficoLinha.getTotal());
			}
    		
    	    graficoLinha.addSeries(series);
		}
   
    	graficoLinha.setLegendPosition("n");
    	graficoLinha.setShowPointLabels(true);
    }
    
    
    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

	public VendaLivro getVendaLivro() {
		return vendaLivro;
	}

	public void setVendaLivro(VendaLivro vendaLivro) {
		this.vendaLivro = vendaLivro;
	}

	public List<Venda> getVendas() {
		return vendas;
	}

	public void setVendas(List<Venda> vendas) {
		this.vendas = vendas;
	}

	public Integer getAno() {
		return ano;
	}

	public void setAno(Integer ano) {
		this.ano = ano;
	}

	public LineChartModel getGraficoLinha() {
		return graficoLinha;
	}

	public void setGraficoLinha(LineChartModel graficoLinha) {
		this.graficoLinha = graficoLinha;
	}
    
    
}
