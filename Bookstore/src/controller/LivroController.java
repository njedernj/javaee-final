package controller;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import dao.LivroDAO;
import entidade.Livro;
import util.MensagemUtil;

@ManagedBean
@RequestScoped
public class LivroController {

	@EJB
    private LivroDAO livroDAO;
    
    private Livro livro;
    
    public LivroController(){
        livro = new Livro();
    }
    
    public void salvar(){
        
        String erro = livroDAO.salvar(livro);
        
        if(erro == null){ 
            livro = new Livro();
            MensagemUtil.addMensagemInfo("Salvo com sucesso!");
        }else{
            MensagemUtil.addMensagemInfo("Erro ao salvar: " + erro);
        }
    }
   
    public void editar(Livro livro){
        this.livro = livro;
    }
    
    public void excluir(Livro livro){
        
        String erro = livroDAO.excluir(livro.getIdLivro());
        
        if(erro == null){
            MensagemUtil.addMensagemInfo("Excluido com sucesso!");
        }else{
            MensagemUtil.addMensagemInfo("Erro ao excluir: " + erro);
        }
    }
    
 
    public List<Livro> listar(){
        return livroDAO.todos();
    }

    public Livro getLivro() {
        return livro;
    }

    public void setLivro(Livro livro) {
        this.livro = livro;
    }
}

