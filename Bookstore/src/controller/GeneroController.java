package controller;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import entidade.Genero;
import dao.GeneroDAO;
import util.MensagemUtil;

@ManagedBean
@RequestScoped
public class GeneroController {
	

		@EJB
	    private GeneroDAO generoDAO;
	    
	    private Genero genero;
	    
	    public GeneroController(){
	    	genero = new Genero();
	    }
	    
	    public void salvar(){
	        
	        String erro = generoDAO.salvar(genero);
	        
	        if(erro == null){ 
	        	genero = new Genero();
	            MensagemUtil.addMensagemInfo("Salvo com sucesso!");
	        }else{
	            MensagemUtil.addMensagemInfo("Erro ao salvar: " + erro);
	        }
	    }
	   
	    public void editar(Genero genero){
	        this.genero = genero;
	    }
	    
	    public void excluir(Genero genero){
	        
	        String erro = generoDAO.excluir(genero.getIdGenero());
	        
	        if(erro == null){
	            MensagemUtil.addMensagemInfo("Excluido com sucesso!");
	        }else{
	            MensagemUtil.addMensagemInfo("Erro ao excluir: " + erro);
	        }
	    }
	    
	    public List<Genero> listar(){
	        return generoDAO.todos();
	    }

	    public Genero getGenero() {
	        return genero;
	    }

	    public void setGenero(Genero genero) {
	        this.genero = genero;
	    }
	}


