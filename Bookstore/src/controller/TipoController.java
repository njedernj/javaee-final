package controller;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import dao.TipoDAO;
import entidade.Tipo;
import util.MensagemUtil;

@ManagedBean
@RequestScoped
public class TipoController {

	@EJB
    private TipoDAO tipoDAO;
    
    private Tipo tipo;
    
    public TipoController(){
        tipo = new Tipo();
    }
    
    public void salvar(){
        
        String erro = tipoDAO.salvar(tipo);
        
        if(erro == null){ 
            tipo = new Tipo();
            MensagemUtil.addMensagemInfo("Salvo com sucesso!");
        }else{
            MensagemUtil.addMensagemInfo("Erro ao salvar: " + erro);
        }
    }
   
    public void editar(Tipo tipo){
        this.tipo = tipo;
    }
    
    public void excluir(Tipo tipo){
        
        String erro = tipoDAO.excluir(tipo.getIdTipo());
        
        if(erro == null){
            MensagemUtil.addMensagemInfo("Excluido com sucesso!");
        }else{
            MensagemUtil.addMensagemInfo("Erro ao excluir: " + erro);
        }
    }
    
    public List<Tipo> listar(){
        return tipoDAO.todos();
    }

    public Tipo getTipo() {
        return tipo;
    }

    public void setTipo(Tipo tipo) {
        this.tipo = tipo;
    }
}