package entidade;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="Venda_Livro")
public class VendaLivro implements Serializable{


	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private VendaLivroPK id;
	
	@NotNull(message="Informe o valor")
	private Double valor;
	
	@NotNull(message="Informe a quantidade de dias")
	private Short quantidade;
	
	@MapsId("idVenda")
	@ManyToOne
	@JoinColumn(name="idVenda")
	private Venda venda;
	
	@MapsId("idLivro")
	@ManyToOne
	@JoinColumn(name="idLivro")
	private Livro livro;


	public VendaLivroPK getId() {
		return id;
	}

	public void setId(VendaLivroPK id) {
		this.id = id;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Short getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Short quantidade) {
		this.quantidade = quantidade;
	}

	public Venda getVenda() {
		return venda;
	}

	public void setVenda(Venda venda) {
		this.venda = venda;
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaLivro other = (VendaLivro) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
