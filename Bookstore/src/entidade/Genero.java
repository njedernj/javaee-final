package entidade;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import entidade.Genero;

@Entity
public class Genero implements Serializable {
	 	
		private static final long serialVersionUID = 1L;
	
		@Id
	    @GeneratedValue(strategy= GenerationType.IDENTITY)
	    private Short idGenero;
	    
	    @NotNull(message="Informe a descri�ao")
	    private String descricao;
	    
	    @OneToMany(mappedBy="genero")
	    private List<Livro> livros;

		public Short getIdGenero() {
			return idGenero;
		}

		public void setIdGenero(Short idGenero) {
			this.idGenero = idGenero;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public List<Livro> getLivros() {
			return livros;
		}

		public void setLivros(List<Livro> livros) {
			this.livros = livros;
		}
	    
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((idGenero == null) ? 0 : idGenero.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Genero other = (Genero) obj;
			if (idGenero == null) {
				if (other.idGenero != null)
					return false;
			} else if (!idGenero.equals(other.idGenero))
				return false;
			return true;
		}
}
