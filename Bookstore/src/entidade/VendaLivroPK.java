package entidade;

import java.io.Serializable;

import javax.persistence.Embeddable;

import entidade.VendaLivroPK;

@Embeddable
public class VendaLivroPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer idVenda;
	private Short idLivro;
	
	//getset
	public Integer getIdVenda() {
		return idVenda;
	}
	public void setIdVenda(Integer idVenda) {
		this.idVenda = idVenda;
	}
	public Short getIdLivro() {
		return idLivro;
	}
	public void setIdLivro(Short idLivro) {
		this.idLivro = idLivro;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idVenda == null) ? 0 : idVenda.hashCode());
		result = prime * result + ((idLivro == null) ? 0 : idLivro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendaLivroPK other = (VendaLivroPK) obj;
		if (idVenda == null) {
			if (other.idVenda != null)
				return false;
		} else if (!idVenda.equals(other.idVenda))
			return false;
		if (idLivro == null) {
			if (other.idLivro != null)
				return false;
		} else if (!idLivro.equals(other.idLivro))
			return false;
		return true;
	}

}