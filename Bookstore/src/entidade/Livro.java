package entidade;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

import entidade.Livro;

@Entity
public class Livro  implements Serializable{


	private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Short idLivro;
    
    @NotNull(message = "Informe a placa")
    private String autor;
    
    @NotNull(message = "Informe o valor")
    private Double valor;
    
    @NotNull(message = "Informe o modelo")
    @ManyToOne
    @JoinColumn(name="idGenero")
    private Genero genero;
    
    @ManyToMany
    @JoinTable(name="livro_genero",
    	joinColumns = @JoinColumn(name="idLivro"),
    	inverseJoinColumns = @JoinColumn(name="idTipo"))
    private List<Tipo> tipos;
    
    @OneToMany(mappedBy="livro")
    private List<VendaLivro> vendaLivros;

	public Short getIdLivro() {
		return idLivro;
	}

	public void setIdLivro(Short idLivro) {
		this.idLivro = idLivro;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public List<Tipo> getTipos() {
		return tipos;
	}

	public void setTipos(List<Tipo> tipos) {
		this.tipos = tipos;
	}

	public List<VendaLivro> getVendaLivros() {
		return vendaLivros;
	}

	public void setVendaLivros(List<VendaLivro> vendaLivros) {
		this.vendaLivros = vendaLivros;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idLivro == null) ? 0 : idLivro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Livro other = (Livro) obj;
		if (idLivro == null) {
			if (other.idLivro != null)
				return false;
		} else if (!idLivro.equals(other.idLivro))
			return false;
		return true;
	}
    
}