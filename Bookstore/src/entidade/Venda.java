package entidade;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.persistence.*;


@Entity
public class Venda implements Serializable {
    
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer idVenda;
   
    @Temporal(TemporalType.TIMESTAMP)
    @NotNull(message= "Informe a data de Venda")
    private Date dataVenda;
    
    private Double valorTotal;
    
    @NotNull(message= "Informe o cliente")
    @ManyToOne
    @JoinColumn(name="idCliente")
    private Cliente cliente;
    
    @OneToMany(mappedBy="venda")
    private List<VendaLivro> VendaLivros;

	public Integer getIdVenda() {
		return idVenda;
	}

	public void setIdVenda(Integer idVenda) {
		this.idVenda = idVenda;
	}

	public Date getDataVenda() {
		return dataVenda;
	}

	public void setDataVenda(Date dataVenda) {
		this.dataVenda = dataVenda;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	

	public List<VendaLivro> getVendaLivros() {
		return VendaLivros;
	}

	public void setVendaLivros(List<VendaLivro> vendaLivros) {
		this.VendaLivros = vendaLivros;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idVenda == null) ? 0 : idVenda.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Venda other = (Venda) obj;
		if (idVenda == null) {
			if (other.idVenda != null)
				return false;
		} else if (!idVenda.equals(other.idVenda))
			return false;
		return true;
	}
    
    
    
    
}
